package CSVReader;

import Etudiant.Etudiant;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVReaderEtudiant {
    String path;

    public CSVReaderEtudiant(String path) {
        this.path = path;
    }

    /**
     * Permet de générer la liste d'étudiants
     * @return
     */
    public List<Etudiant> loadEtudiants() {
        List<Etudiant> liEtu = new ArrayList<Etudiant>();
        try {
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (!data.startsWith("id")) {
                    String[] tab = data.split(";");
                    liEtu.add(new Etudiant(tab[2], tab[1], tab[0], tab[3]));
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return liEtu;
    }
}
