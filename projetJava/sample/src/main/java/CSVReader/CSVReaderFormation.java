package CSVReader;

import Formation.Formation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVReaderFormation {String path;

    public CSVReaderFormation(String path) {
        this.path = path;
    }
    /**
     * Permet de générer la liste de formations
     * @return
     */

    public List<Formation> loadEtudiants() {
        List<Formation> liFor = new ArrayList<Formation>();
        try {
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (!data.startsWith("id")) {
                    String[] tab = data.split(";");
                    liFor.add(new Formation(tab[0], tab[1]));
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return liFor;
    }

}
