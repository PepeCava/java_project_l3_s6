package CSVReader;

import Formation.Mention;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVReaderMention {
    String path;

    public CSVReaderMention(String path) {
        this.path = path;
    }

    /**
     * Permet de générer la liste de mentions
     * @return
     */
    public List<Mention> loadEtudiants() {
        List<Mention> liMention = new ArrayList<Mention>();
        try {
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (!data.startsWith("id")) {
                    String[] tab = data.split(";");
                    liMention.add(new Mention(tab[1], tab[0], tab[2]));
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return liMention;
    }
}
