package CSVReader;

import Formation.Parcours;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVReaderParcours {
    String path;

    public CSVReaderParcours(String path) {
        this.path = path;
    }

    /**
     * Permet de générer la liste de parcours
     *
     * @return
     */
    public List<Parcours> loadEtudiants() {
        List<Parcours> liEtu = new ArrayList<Parcours>();
        try {
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (!data.startsWith("id")) {
                    String[] tab = data.split(";");
                    liEtu.add(new Parcours(tab[1], tab[0], tab[2]));
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return liEtu;
    }
}
