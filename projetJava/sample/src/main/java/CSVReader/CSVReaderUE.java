package CSVReader;

import Formation.UE;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVReaderUE {
    String path;

    public CSVReaderUE(String path) {
        this.path = path;
    }

    /**
     * Permet de générer la liste d'UE
     * @return
     */
    public List<UE> loadEtudiants() {
        List<UE> liEtu = new ArrayList<UE>();
        try {
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (!data.startsWith("id")) {
                    String[] tab = data.split(";");
                    UE ue = new UE(tab[1], tab[0], tab[4]);
                    ue.setOuverture(tab[2].equals("OUI"));
                    ue.setCreditECT(Integer.parseInt(tab[3]));
                    for(int i = 5; i< tab.length; i++){
                        ue.addUePrerequis(tab[i]);
                    }
                    liEtu.add(ue);
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return liEtu;
    }
}
