package CSVReader;

import Etudiant.Etudiant;
import Formation.UE;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class CSVReaderUEEnCours {
    String path;

    public CSVReaderUEEnCours(String path) {
        this.path = path;
    }

    /**
     * Permet d'indiquer les UE en cours des étudiants
     * @param li liste d'étudiants
     */
    public void loadUEEnCours(List<Etudiant> li) {
        try {
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (!data.startsWith("etudiant_id")) {
                    String[] tab = data.split(";");
                    Etudiant et = null;
                    for(Etudiant e :li) {
                        if (e.getINE().equals(tab[0])) {
                            et = e;
                            break;
                        }
                    }
                    if(et != null){
                        et.addUEEnCours(tab[1]);
                    }
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

}
