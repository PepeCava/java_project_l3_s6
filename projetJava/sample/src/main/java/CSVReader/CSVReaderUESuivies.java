package CSVReader;

import Etudiant.Etudiant;
import Formation.UE;
import Formation.UESuivies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVReaderUESuivies {
    String path;

    public CSVReaderUESuivies(String path) {
        this.path = path;
    }

    /**
     * Permet d'indiquer les UE qui ont été suivies des étudiants
     * @param liEtu liste d'étudiants
     * @param liUE liste d'ue
     */
    public void loadUESuivies(List<Etudiant> liEtu, List<UE> liUE) {
        try {
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (!data.startsWith("etudiant_id")) {
                    String[] tab = data.split(";");
                    Etudiant et = null;
                    UE ue = null;
                    for(Etudiant e : liEtu){
                        if(e.getINE().equals(tab[0])) {
                            et = e;
                            break;
                        }
                    }

                    for(UE u : liUE){
                        if(u.getCodeUE().equals(tab[1])) {
                            ue = u;
                            break;
                        }
                    }
                    if(ue != null){
                        if(et != null){
                            UESuivies ues = new UESuivies(ue, tab[2], tab[3], tab[4].equals("OUI"));
                            et.addUESuivie(ues);
                        }
                    }
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }


}
