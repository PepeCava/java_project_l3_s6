package CSVWriter;

import Etudiant.Etudiant;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CSVWriterEtudiant {
    String path;

    public CSVWriterEtudiant(String path) {
        this.path = path;
    }

    /**
     * Méthode permettant de générer le CSV des étudiants
     * @param li liste d'étudiants
     * @throws IOException
     */
    public void writeEtudiant(List<Etudiant> li) throws IOException {
        String val = "id;prenom;nom;parcours_id\n";
        for (Etudiant e : li)
            val += e.getINE() + ";" + e.getPrenom() + ";" + e.getNom() + ";" + e.getIdParcours() + "\n";
        FileWriter f2 = new FileWriter(path, false);
        f2.write(val);
        f2.close();
    }
}
