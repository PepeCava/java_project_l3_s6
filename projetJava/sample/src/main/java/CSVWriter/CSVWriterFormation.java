package CSVWriter;

import Formation.Formation;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CSVWriterFormation {
    String path;

    public CSVWriterFormation(String path) {
        this.path = path;
    }

    /**
     * Méthode permettant de générer le CSV des formations
     * @param li liste de formations
     * @throws IOException
     */
    public void writeFormation(List<Formation> li) throws IOException {
        String val = "id;nom\n";
        for (Formation f : li)
            val += f.getID() + ";" + f.getNom() + "\n";
        FileWriter f2 = new FileWriter(path, false);
        f2.write(val);
        f2.close();
    }
}
