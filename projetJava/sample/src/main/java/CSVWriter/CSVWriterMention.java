package CSVWriter;

import Formation.Mention;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CSVWriterMention {
    String path;

    public CSVWriterMention(String path) {
        this.path = path;
    }
    /**
     * Méthode permettant de générer le CSV des mentions
     * @param li liste de mentions
     * @throws IOException
     */

    public void writeMention(List<Mention> li) throws IOException {
        String val = "id;nom\n";
        for (Mention m : li)
            val += m.getId() + ";" + m.getNom() + ";" + m.getFormationID() + "\n";
        FileWriter f2 = new FileWriter(path, false);
        f2.write(val);
        f2.close();
    }
}
