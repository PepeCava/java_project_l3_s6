package CSVWriter;

import Formation.Parcours;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CSVWriterParcours {
    String path;

    public CSVWriterParcours(String path) {
        this.path = path;
    }

    /**
     * Méthode permettant de générer le CSV des parcours
     * @param li liste de parcours
     * @throws IOException
     */
    public void writeParcours(List<Parcours> li) throws IOException {
        String val = "id;prenom;nom;parcours_id\n";
        for (Parcours p : li)
            val += p.getId() + ";" + p.getNom() + ";" + p.getMention_ID() + "\n";
        FileWriter f2 = new FileWriter(path, false);
        f2.write(val);
        f2.close();
    }
}
