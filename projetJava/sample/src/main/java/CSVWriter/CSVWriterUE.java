package CSVWriter;

import Formation.UE;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CSVWriterUE {
    String path;

    public CSVWriterUE(String path) {
        this.path = path;
    }
    /**
     * Méthode permettant de générer le CSV des UE
     * @param li liste d'UE
     * @throws IOException
     */
    public void writeUE(List<UE> li) throws IOException {
        String val = "id;prenom;nom;parcours_id\n";
        for (UE ue : li) {
            val += ue.getCodeUE() + ";" + ue.getNom() + ";";
            val += ue.isOuverture() ? "OUI;" : "NON;";
            val += ue.getCreditECT() + ";" + ue.getParcoursId() + ";";
            for(String s : ue.getListPrerequis())
                val+=s+";";
            if(val.endsWith(";"))
                val = val.substring(0, val.length() - 1);
            val += "\n";
        }
        FileWriter f2 = new FileWriter(path, false);
        f2.write(val);
        f2.close();
    }
}
