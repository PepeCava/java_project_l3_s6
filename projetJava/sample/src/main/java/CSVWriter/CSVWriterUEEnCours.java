package CSVWriter;

import Etudiant.Etudiant;
import Formation.UESuivies;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CSVWriterUEEnCours {
    String path;

    public CSVWriterUEEnCours(String path) {
        this.path = path;
    }

    /**
     * Méthode permettant de générer le CSV des UE en cours par l'ensemble des étudiants
     * @param li liste d'étudiant
     * @throws IOException
     */
    public void writeUEEnCours(List<Etudiant> li) throws IOException {
        String val = "";
        for (Etudiant e : li) {
            for(String s : e.getLiUEEnCours()){
                val += e.getINE()+";"+s+"\n";
            }
        }

        FileWriter f2 = new FileWriter(path, false);
        f2.write(val);
        f2.close();
    }
}
