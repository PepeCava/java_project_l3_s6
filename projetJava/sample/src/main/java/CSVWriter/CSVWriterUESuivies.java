package CSVWriter;

import Etudiant.Etudiant;
import Formation.UE;
import Formation.UESuivies;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CSVWriterUESuivies {
    String path;

    public CSVWriterUESuivies(String path) {
        this.path = path;
    }

    /**
     * Méthode permettant de générer le CSV des UE suvies par l'ensemble des étudiants
     * @param li liste d'étudiant
     * @throws IOException
     */
    public void writeUESuivies(List<Etudiant> li) throws IOException {
        String val = "";
        for (Etudiant e : li) {
            for (UESuivies ues : e.getListUESuivies()) {
                val += e.getINE() + ";" + ues.getUe().getCodeUE() + ";" + ues.getAnnee() + ";" + ues.getSemestre() + ";" + (ues.isValidee() ? "OUI" : "NON") + "\n";
            }
        }

        FileWriter f2 = new FileWriter(path, false);
        f2.write(val);
        f2.close();
    }

}
