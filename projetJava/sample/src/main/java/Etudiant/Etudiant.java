package Etudiant;

import Formation.Parcours;
import Formation.UE;
import Formation.UESuivies;
import GUI.MainClass;

import java.util.ArrayList;
import java.util.List;

/**
 * @author pierrepaul
 */
public class Etudiant {

    private String nom;
    private String prenom;
    private String INE;
    private String idParcours;
    private List<UESuivies> listUESuivies;
    private List<String> liUEEnCours;

    /**
     * Constructeur Etudiant
     *
     * @param nom        nom de l'étudiant
     * @param prenom     prenom de l'étudiant
     * @param INE        INE de l'étudiant
     * @param idParcours id du parcours
     */
    public Etudiant(String nom, String prenom, String INE, String idParcours) {
        this.nom = nom;
        this.prenom = prenom;
        this.INE = INE;
        this.idParcours = idParcours;
        listUESuivies = new ArrayList<UESuivies>();
        liUEEnCours = new ArrayList<String>();
    }

    /**
     * Constructeur Etudiant
     *
     * @param nom    nom de l'étudiant
     * @param prenom prenom de l'étudiant
     * @param INE    INE de l'étudiant
     */
    public Etudiant(String nom, String prenom, String INE) {
        this.nom = nom;
        this.prenom = prenom;
        this.INE = INE;
        this.idParcours = "-1";
        listUESuivies = new ArrayList<UESuivies>();
        liUEEnCours = new ArrayList<String>();
    }

    /**
     * Getter Id Parcours
     *
     * @return idParcours
     */
    public String getIdParcours() {
        return idParcours;
    }

    /**
     * Setter ID Parcours
     *
     * @param idParcours l'id du parcours
     */
    public void setIdParcours(String idParcours) {
        this.idParcours = idParcours;
    }

    /**
     * Getter nom
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    public List<UESuivies> getListUESuivies() {
        return listUESuivies;
    }

    public List<String> getLiUEEnCours() {
        return liUEEnCours;
    }

    /**
     * Setter nom
     *
     * @param nom nom de l'étudiant
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * getter prenom
     *
     * @return prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Setter prenom
     *
     * @param prenom prénom de l'étudiant
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Getter INE
     *
     * @return INE
     */
    public String getINE() {
        return INE;
    }

    /**
     * Setter INE
     *
     * @param iNE INE de l'étudiant
     */
    public void setINE(String iNE) {
        INE = iNE;
    }

    public void addUESuivie(UESuivies u) {
        listUESuivies.add(u);
    }

    public void addUEEnCours(String u) {
        liUEEnCours.add(u);
    }

    @Override
    public String toString() {
        return "Etudiant [nom=" + nom + ", prenom=" + prenom + ", INE=" + INE /*+ ", formation=" + formation
				+ ", listUESuivies=" + listUESuivies + "]"*/;
    }

    /**
     * Retourne le nom du parcours d'un étudiant
     * @param e un étudiant
     * @return
     */
    public static String getETParcoursName(Etudiant e) {
        for (Parcours p : MainClass.liParcours) {
            if (e.getIdParcours().equals(p.getId())) {
                return p.getNom();
            }
        }
        return "";
    }

    /**
     * Retourne la liste des UE en cours d'un étudiant
     * @param e un étudiant
     * @return
     */
    public static List<UE> getUEEnCours(Etudiant e) {
        List<UE> liUe = new ArrayList<>();
        for (String s : e.getLiUEEnCours()) {
            for (UE ue : MainClass.liUE) {
                if (ue.getCodeUE().equals(s)) {
                    liUe.add(ue);
                }
            }
        }
        return liUe;
    }

    /**
     * Permet de savoir si une UE est validée à partir de son code UE
     * @param s le code d'une UE
     * @return
     */
    private boolean isValidated(String s) {
        for (UESuivies ues : listUESuivies)
            if (ues.getUe().getCodeUE().equals(s) && ues.isValidee())
                return true;
        return false;
    }

    /**
     * Retourne la liste des UE envisageables par l'étudiant
     * @return
     */
    public List<UE> getUEEnvisageable() {
        List<UE> liUE = new ArrayList<UE>();
        for (UE ue : MainClass.liUE) {
            boolean isConceivable = true;
            if (liUEEnCours.contains(ue.getCodeUE())) {
                isConceivable = false;
            } else if (isValidated(ue.getCodeUE())) {
                isConceivable = false;
            } else {
                for (String s : ue.getListPrerequis()) {
                    if (!liUEEnCours.contains(s) && !isValidated(s)) {
                        isConceivable = false;
                        break;
                    }
                }
                if (isConceivable)
                    liUE.add(ue);
            }
        }
        return liUE;
    }

    /**
     * Retourne la liste des UE validées par l'étudiant
     * @return
     */
    public List<UESuivies> getUeValidee() {
        List<UESuivies> liues = new ArrayList<>();
        for (UESuivies ues : listUESuivies)
            if (ues.isValidee())
                liues.add(ues);
        return liues;
    }

    public static List<Etudiant> getEtudiantSuivantUneUE(UE ue){
        List<Etudiant> liEtu = new ArrayList<Etudiant>();

        for(Etudiant e : MainClass.liEtu){
            if(e.getLiUEEnCours().contains(ue.getCodeUE()))
                liEtu.add(e);
        }
        return liEtu;
    }

}
