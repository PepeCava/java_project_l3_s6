package Etudiant;

import javafx.beans.property.SimpleBooleanProperty;

import java.util.ArrayList;
import java.util.List;

public class EtudiantValidation extends Etudiant{
    private SimpleBooleanProperty isValidating = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty hasValidated = new SimpleBooleanProperty(false);

    public EtudiantValidation(String nom, String prenom, String INE, String idParcours) {
        super(nom, prenom, INE, idParcours);
    }

    public boolean isIsValidating() {
        return isValidating.get();
    }

    public SimpleBooleanProperty isValidatingProperty() {
        return isValidating;
    }

    public void setIsValidating(boolean isValidating) {
        this.isValidating.set(isValidating);
    }

    public boolean isHasValidated() {
        return hasValidated.get();
    }

    public SimpleBooleanProperty hasValidatedProperty() {
        return hasValidated;
    }

    public void setHasValidated(boolean hasValidated) {
        this.hasValidated.set(hasValidated);
    }

    /**
     * Crée une liste d'étudiantValidation à partir d'une liste d'étudiants
     * @param li liste d'étudiants
     * @return
     */
    public static List<EtudiantValidation> getEtuValidation(List<Etudiant> li){
        List<EtudiantValidation> lietuv = new ArrayList<EtudiantValidation>();
        for(Etudiant e : li){
            lietuv.add(new EtudiantValidation(e.getNom(), e.getPrenom(), e.getINE(), e.getIdParcours()));
        }
        return lietuv;
    }
}
