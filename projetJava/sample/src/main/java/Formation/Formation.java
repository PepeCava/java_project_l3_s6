package Formation;

/**
 * 
 * @author pierrepaul
 *
 */
public class Formation {
	String ID;
	String nom;

	public Formation(String id, String nom) {
		this.nom = nom;
		this.ID = id;
	}

	public String getNom() {
		return nom;
	}

	@Override
	public String toString() {
		return "Formation{" +
				"ID='" + ID + '\'' +
				", nom='" + nom + '\'' +
				'}';
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}
}
