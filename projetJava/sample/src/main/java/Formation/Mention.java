package Formation;

public class Mention {

	String nom;
	String id;
	String FormationID;

	public Mention(String nom, String id, String formationID) {
		this.nom = nom;
		this.id = id;
		this.FormationID = formationID;
	}

	@Override
	public String toString() {
		return "Mention{" +
				"nom='" + nom + '\'' +
				", id='" + id + '\'' +
				", FormationID='" + FormationID + '\'' +
				'}';
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFormationID() {
		return FormationID;
	}

	public void setFormationID(String formationID) {
		FormationID = formationID;
	}
}
