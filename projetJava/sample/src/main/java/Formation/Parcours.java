package Formation;

import GUI.MainClass;

public class Parcours {

	String nom; 
	String id;
	String mention_ID;

	public Parcours(String nom, String id, String mention_ID) {
		this.nom = nom;
		this.id = id;
		this.mention_ID = mention_ID;
	}

	public String getNom() {
		return nom;
	}

	@Override
	public String toString() {
		return getNom();
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMention_ID() {
		return mention_ID;
	}

	public void setMention_ID(String mention_ID) {
		this.mention_ID = mention_ID;
	}

	/**
	 * Retourne le nom de la mention d'un parcours
	 * @param p un parcours
	 * @return
	 */
	public static String getMentionName(Parcours p){
		for(Mention m : MainClass.liMention){
			if(p.getMention_ID().equals(m.getId()))
				return m.getNom();
		}
		return "";
	}
}
