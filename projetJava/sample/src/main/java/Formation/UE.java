package Formation;

import GUI.MainClass;

import java.util.ArrayList;
import java.util.List;


public class UE {

    String nom;
    String codeUE;
    boolean ouverture;
    int creditECT;
    String parcoursId;
    List<String> listPrerequis;

    public String getParcoursId() {
        return parcoursId;
    }

    public void setParcoursId(String parcoursId) {
        this.parcoursId = parcoursId;
    }


    public UE(String nom, String codeUE, String parcoursId) {
        this.nom = nom;
        this.codeUE = codeUE;
        this.parcoursId = parcoursId;
        listPrerequis = new ArrayList<String>();
    }


    public String getNom() {
        return nom;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }


    public String getCodeUE() {
        return codeUE;
    }


    public void setCodeUE(String codeUE) {
        this.codeUE = codeUE;
    }


    public boolean isOuverture() {
        return ouverture;
    }


    public void setOuverture(boolean ouverture) {
        this.ouverture = ouverture;
    }

    public int getCreditECT() {
        return creditECT;
    }

    public void setCreditECT(int creditECT) {
        this.creditECT = creditECT;
    }


    public List<String> getListPrerequis() {
        return listPrerequis;
    }


    public void setListPrerequis(List<String> listPrerequis) {
        this.listPrerequis = listPrerequis;
    }

    public void addUePrerequis(String codeUe) {
        listPrerequis.add(codeUe);
    }


    @Override
    public String toString() {
        return "UE{" +
                "nom='" + nom + '\'' +
                ", codeUE='" + codeUE + '\'' +
                ", ouverture=" + ouverture +
                ", creditECT=" + creditECT +
                ", parcoursId='" + parcoursId + '\'' +
                ", listPrerequis=" + listPrerequis +
                '}';
    }

    /**
     * Retourne le nom du parcours d'une UE
     * @param ue une UE
     * @return
     */
    public static String getUEParcoursName(UE ue) {
        for (Parcours p : MainClass.liParcours) {
            if (p.getId().equals(ue.getParcoursId())) {
                return p.getNom();
            }
        }
        return "";
    }

    /**
     * Retourne le nom de la mention d'une UE
     * @param ue une UE
     * @return
     */
    public static String getUEMentionName(UE ue) {
        for (Parcours p : MainClass.liParcours) {
            if (p.getId().equals(ue.getParcoursId())) {
                return Parcours.getMentionName(p);
            }
        }
        return "";
    }

    /**
     * Retourne le nom des prérequis d'une UE
     * @param ue une UE
     * @return
     */
    public static String getPrerequisName(UE ue) {
        String retvalue = "";
        for (String s : ue.getListPrerequis()) {
            for (UE u : MainClass.liUE) {
                if (s.equals(u.codeUE)) {
                    retvalue += u.getNom() + " ";
                    break;
                }
            }
        }
        return retvalue.length() > 0 ? retvalue : "Aucun";
    }
}
