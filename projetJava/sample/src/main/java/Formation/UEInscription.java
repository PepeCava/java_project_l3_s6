package Formation;

import javafx.beans.property.SimpleBooleanProperty;

import java.util.ArrayList;
import java.util.List;

public class UEInscription extends UE{
    private SimpleBooleanProperty isInscrit = new SimpleBooleanProperty(false);

    public UEInscription(String nom, String codeUE, String parcoursId) {
        super(nom, codeUE, parcoursId);
    }

    public boolean getIsInscrit() {
        return isInscrit.get();
    }

    public SimpleBooleanProperty isInscritProperty() {
        return isInscrit;
    }

    public void setIsInscrit(boolean isInscrit) {
        this.isInscrit.set(isInscrit);
    }

    /**
     * Transforme une liste d'UE en liste d'UEInscrption
     * @param li Liste d'UE
     * @return
     */
    public static List<UEInscription> getUEInscriptions(List<UE> li){
        List<UEInscription> liuei = new ArrayList<UEInscription>();
        for(UE u : li) {
            UEInscription ue = new UEInscription(u.getNom(), u.getCodeUE(), u.getParcoursId());
            ue.setCreditECT(u.getCreditECT());
            ue.setListPrerequis(u.getListPrerequis());
            liuei.add(ue);
        }
        return liuei;
    }
}
