package Formation;

import Etudiant.Etudiant;
import GUI.MainClass;

public class UESuivies {
    private UE ue;
    private String annee;
    private String semestre;
    private boolean validee;

    public UESuivies(UE ue, String annee, String semestre, boolean validee){
        this.ue = ue;
        this.annee = annee;
        this.semestre = semestre;
        this.validee = validee;
    }

    public UE getUe() {
        return ue;
    }

    public void setUe(UE ue) {
        this.ue = ue;
    }

    public String getAnnee() {
        return annee;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public boolean isValidee() {
        return validee;
    }

    public void setValidee(boolean validee) {
        this.validee = validee;
    }

    public static String getUEName(UESuivies u) {
        return u.getUe().getNom();
    }

    public static String getUEECT(UESuivies u) {
        return u.getUe().getCreditECT()+"";
    }

    /**
     * Retourne le nom du parcours de l'UE
     * @param u UESuivies
     * @return
     */
    public static String getParcoursNameUES(UESuivies u) {
        for (Parcours p : MainClass.liParcours) {
            if (u.getUe().getParcoursId().equals(p.getId())) {
                return p.getNom();
            }
        }
        return "";
    }
}
