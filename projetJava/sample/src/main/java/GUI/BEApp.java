package GUI;

import Etudiant.Etudiant;
import Formation.UE;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class BEApp extends Application implements Initializable {
    @FXML
    public TableView tbUE;
    @FXML
    public TableColumn<UE, String> colNomUE;
    @FXML
    public TableColumn<UE, String> colCodeUE;
    @FXML
    public TableColumn<UE, String> colOuverture;
    @FXML
    public TableColumn<UE, String> creditECT;


    public  static  UE ue;
    @Override



    public void start(Stage primaryStage) throws IOException {
        Parent root = (Parent) FXMLLoader.load(this.getClass().getResource("BEApp.fxml"));
        Scene scene = new Scene(root, 601.0D, 377.0D);
        primaryStage.setTitle("Bureau des etudes");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Charge la liste d'ue
     */
    public void dgvLoadUE() {
        tbUE.getItems().removeAll();
        colNomUE.setCellValueFactory(new PropertyValueFactory<UE, String>("nom"));
        colCodeUE.setCellValueFactory(new PropertyValueFactory<UE, String>("CodeUE"));

        tbUE.setItems(FXCollections.observableList(MainClass.liUE));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dgvLoadUE();
    }

    @FXML
    private void handleButtonBEAction(ActionEvent event) throws IOException {
        MainClass.setRoot("MainClass.fxml");
    }
    @FXML
    private void handleChangePathMenuItem(ActionEvent event) throws IOException {
        MainClass.pathChooser();
    }

    @FXML
    private void handleCloseMenuItem(ActionEvent event) throws IOException {
        Platform.exit();
    }

    @FXML
    public void clickItem(MouseEvent event) throws Exception {
        if (event.getClickCount() == 2) {
            ue = ((UE) tbUE.getSelectionModel().getSelectedItem());
            new BEVisualisationUE().start(new Stage());
        }
    }

}
