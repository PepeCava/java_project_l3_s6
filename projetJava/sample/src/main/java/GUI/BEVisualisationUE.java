package GUI;

import Etudiant.*;
import Formation.UE;
import Formation.UEInscription;
import Formation.UESuivies;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class BEVisualisationUE extends Application implements Initializable {
    @FXML
    public TableView tv;
    @FXML
    public Button btSave;
    @FXML
    public TableColumn<EtudiantValidation, String> colIne;
    @FXML
    public TableColumn<EtudiantValidation, String> colNom;
    @FXML
    public TableColumn<EtudiantValidation, String> colPrenom;
    @FXML
    public TableColumn<EtudiantValidation, Boolean> colIsValidating;
    @FXML
    public TableColumn<EtudiantValidation, Boolean> colhasValidated;
    public boolean hasBeenSaved = false;
    private static Stage stage;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loaddgv();

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                if (!hasBeenSaved) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Fermeture de la fenêtre");
                    alert.setContentText("Etes-vous sûr de vouloir fermer sans sauvegarder ?");
                    Optional<ButtonType> result = alert.showAndWait();
                    if (!((result.isPresent()) && (result.get() == ButtonType.OK))) {
                        we.consume();
                    }
                }
            }
        });
    }

    public void start(Stage primaryStage) throws IOException {
        stage = primaryStage;
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("BEVisualisationUE.fxml")), 700, 380);
        primaryStage.setScene(scene);
        primaryStage.setScene(scene);
        Image img = new Image("UPS.jpg");
        primaryStage.setTitle(BEApp.ue.getNom() + " : Valider les résultats des étudiants");
        primaryStage.show();
    }

    /**
     * Charge la tableView
     */
    private void loaddgv() {

        tv.getItems().removeAll();
        tv.setEditable(true);
        colIne.setCellValueFactory(new PropertyValueFactory<EtudiantValidation, String>("INE"));
        colNom.setCellValueFactory(new PropertyValueFactory<EtudiantValidation, String>("nom"));
        colPrenom.setCellValueFactory(new PropertyValueFactory<EtudiantValidation, String>("prenom"));
        colIsValidating.setCellValueFactory(new PropertyValueFactory<EtudiantValidation, Boolean>("isValidating"));
        colIsValidating.setCellFactory(tc -> new CheckBoxTableCell<>());
        colIsValidating.setEditable(true);
        colhasValidated.setCellValueFactory(new PropertyValueFactory<EtudiantValidation, Boolean>("hasValidated"));
        colhasValidated.setCellFactory(tc -> new CheckBoxTableCell<>());
        colhasValidated.setEditable(true);
        tv.setItems(FXCollections.observableList(EtudiantValidation.getEtuValidation(Etudiant.getEtudiantSuivantUneUE(BEApp.ue))));
    }

    /**
     * Edition des UE validée ou non
     */
    private void saveChanges() {
        List<EtudiantValidation> lietuv = tv.getItems();
        for (EtudiantValidation e : lietuv) {
            if (e.isIsValidating()) {
                Etudiant etu = null;
                for (Etudiant et : MainClass.liEtu) {
                    if (et.getINE().equals(e.getINE())) {
                        etu = et;
                        break;
                    }
                }
                UESuivies ues = new UESuivies(BEApp.ue, MainClass.currentYear, MainClass.currentSemester, e.isHasValidated());
                etu.getLiUEEnCours().remove(BEApp.ue.getCodeUE());
                etu.getListUESuivies().add(ues);
            }
        }
    }

    /**
     * Enregistre les modifications dans les CSV
     * @param e ActionEvent
     */
    public void handleBtSave(ActionEvent e) {
        try {
            saveChanges();
            MainClass.writeOnCSV();
            hasBeenSaved = true;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sauvegarde Réussie");
            alert.setContentText("Sauvergarde effectuée avec succès");
            alert.show();
        } catch (IOException ex) {

        }
    }
}

