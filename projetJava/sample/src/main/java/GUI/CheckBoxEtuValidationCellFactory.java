package GUI;

import Etudiant.EtudiantValidation;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;

public class CheckBoxEtuValidationCellFactory implements Callback {
    @Override
    public TableCell call(Object param) {
        CheckBoxTableCell<EtudiantValidation,Boolean> checkBoxCell = new CheckBoxTableCell();
        return checkBoxCell;
    }
}
