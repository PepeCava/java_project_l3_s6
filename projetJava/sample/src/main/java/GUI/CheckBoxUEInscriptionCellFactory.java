package GUI;

import Formation.UEInscription;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;

public class CheckBoxUEInscriptionCellFactory implements Callback {
    @Override
    public TableCell call(Object param) {
        CheckBoxTableCell<UEInscription,Boolean> checkBoxCell = new CheckBoxTableCell();
        return checkBoxCell;
    }
}