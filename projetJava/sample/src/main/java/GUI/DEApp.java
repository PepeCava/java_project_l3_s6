package GUI;

import Etudiant.Etudiant;
import Formation.Parcours;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class DEApp extends Application implements Initializable {
    @FXML
    public TableView tbET;
    @FXML
    public TableColumn<Etudiant, String> colINE;
    @FXML
    public TableColumn<Etudiant, String> colNom;
    @FXML
    public TableColumn<Etudiant, String> colPrenom;
    @FXML
    public TableColumn<Etudiant, String> colParcours;

    public static Etudiant et;

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = (Parent) FXMLLoader.load(this.getClass().getResource("DEApp.fxml"));
        Scene scene = new Scene(root, 601.0D, 377.0D);
        primaryStage.setTitle("Directeur des études");
        primaryStage.setScene(scene);
        primaryStage.show();
        //dgvLoadEtu();
    }

    /**
     * Charge la liste d'étudiants
     */
    public void dgvLoadEtu() {
        tbET.getItems().removeAll();
        colINE.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("INE"));
        colNom.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("nom"));
        colPrenom.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("prenom"));
        colParcours.setCellValueFactory(cellData -> {
            String parcoursName = Etudiant.getETParcoursName(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });
        tbET.setItems(FXCollections.observableList(MainClass.liEtu));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dgvLoadEtu();
    }

    @FXML
    private void handleButtonBEAction(ActionEvent event) throws IOException {
        MainClass.setRoot("MainClass.fxml");
    }
    @FXML
    private void handleChangePathMenuItem(ActionEvent event) throws IOException {
        MainClass.pathChooser();
    }

    @FXML
    private void handleCloseMenuItem(ActionEvent event) throws IOException {
        Platform.exit();
    }

    @FXML
    public void clickItem(MouseEvent event) throws IOException {
        if (event.getClickCount() == 2) {
            et = ((Etudiant) tbET.getSelectionModel().getSelectedItem());
            new DEVisualisationEtudiant().start(new Stage());
        }
    }


}
