package GUI;

import Etudiant.Etudiant;
import Formation.UE;
import Formation.UESuivies;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class DEVisualisationEtudiant extends Application implements Initializable {
    @FXML
    public TableView tv1;
    @FXML
    public TableView tv2;
    @FXML
    public TableView tv3;
    @FXML
    public TableColumn<UESuivies, String> uevColUE;
    @FXML
    public TableColumn<UESuivies, String> uevColECT;
    @FXML
    public TableColumn<UESuivies, String> uevColParcours;
    @FXML
    public TableColumn<UESuivies, String> uevColUEAnneeValid;
    @FXML
    public TableColumn<UESuivies, String> uevColSem;
    @FXML
    public TableColumn<UE, String> uecColUE;
    @FXML
    public TableColumn<UE, String> uecColECT;
    @FXML
    public TableColumn<UE, String> uecColParcours;
    @FXML
    public TableColumn<UE, String> ueeColUE;
    @FXML
    public TableColumn<UE, String> ueeColECT;
    @FXML
    public TableColumn<UE, String> ueeColParcours;
    @FXML
    public TableColumn<UE, String> ueeColPre;
    @FXML
    public TableColumn<UE, String> ueeColMention;

    private Stage stage;

    @Override
    public void start(Stage primaryStage) throws IOException {
        stage = primaryStage;
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("DEVisualisationEtudiant.fxml")), 800, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle(DEApp.et.getNom() + " " + DEApp.et.getPrenom() + " : Vue d'ensemble sur les UE");
        Image img = new Image("UPS.jpg");
        primaryStage.getIcons().add(img);
        primaryStage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loaddgv1();
        loaddgv2();
        loaddgv3();
    }

    /**
     * Charge la tableView des UE validées
     */
    private void loaddgv1() {

        tv1.getItems().removeAll();
        uevColUE.setCellValueFactory(cellData -> {
            String ueName = UESuivies.getUEName(cellData.getValue());
            return new SimpleStringProperty(ueName);
        });
        uevColECT.setCellValueFactory(cellData -> {
            String ueect = UESuivies.getUEECT(cellData.getValue());
            return new SimpleStringProperty(ueect);
        });
        uevColParcours.setCellValueFactory(cellData -> {
            String parcoursName = UESuivies.getParcoursNameUES(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });
        uevColUEAnneeValid.setCellValueFactory(new PropertyValueFactory<UESuivies, String>("annee"));

        uevColSem.setCellValueFactory(new PropertyValueFactory<UESuivies, String>("semestre"));
        tv1.setItems(FXCollections.observableList(DEApp.et.getUeValidee()));
    }

    /**
     * Charge la tableView des UE en cours
     */
    private void loaddgv2() {
        tv2.getItems().removeAll();
        uecColUE.setCellValueFactory(new PropertyValueFactory<UE, String>("nom"));
        uecColECT.setCellValueFactory(new PropertyValueFactory<UE, String>("creditECT"));
        uecColParcours.setCellValueFactory(cellData -> {
            String parcoursName = UE.getUEParcoursName(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });
        tv2.setItems(FXCollections.observableList(Etudiant.getUEEnCours(DEApp.et)));
    }

    /**
     * Charge la tableView des UE envisageables pour l'étudiant
     */
    private void loaddgv3() {

        tv3.getItems().removeAll();
        ueeColUE.setCellValueFactory(new PropertyValueFactory<UE, String>("nom"));
        ueeColECT.setCellValueFactory(new PropertyValueFactory<UE, String>("creditECT"));
        ueeColParcours.setCellValueFactory(cellData -> {
            String parcoursName = UE.getUEParcoursName(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });

        ueeColPre.setCellValueFactory(cellData -> {
            String parcoursName = UE.getPrerequisName(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });
        ueeColMention.setCellValueFactory(cellData -> {
            String parcoursName = UE.getUEMentionName(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });
        tv3.setItems(FXCollections.observableList(DEApp.et.getUEEnvisageable()));
    }
}
