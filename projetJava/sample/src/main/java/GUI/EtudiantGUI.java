package GUI;

import Etudiant.Etudiant;
import Formation.Parcours;
import Formation.UE;
import Formation.UEInscription;
import Formation.UESuivies;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class EtudiantGUI extends Application implements Initializable {
    @FXML
    public TextField tfINE;
    @FXML
    public AnchorPane mainPane;
    @FXML
    public TableView tv;
    @FXML
    public TextField tfNom;
    @FXML
    public TextField tfPrenom;
    @FXML
    public Button btAdd;
    @FXML
    public Button btSave;
    @FXML
    public Button btDelete;
    @FXML
    public ComboBox<Parcours> cbParcours;
    @FXML
    public TableColumn<UEInscription, String> ColUE;
    @FXML
    public TableColumn<UEInscription, String> ColECT;
    @FXML
    public TableColumn<UEInscription, String> ColParcours;
    @FXML
    public TableColumn<UEInscription, String> ColPre;
    @FXML
    public TableColumn<UEInscription, String> ColMention;
    @FXML
    public TableColumn<UEInscription, Boolean> ColInscrire;

    public boolean hasBeenSaved = false;
    private int v = 1200;
    private int v1 = 400;

    private static Stage sta;

    public void start(Stage stage) throws Exception {
        sta = stage;
        String title = "";
        if (SPApp.e == null) {
            v = 350;
            v1 = 400;
            title = "Ajouter un étudiant";
        } else {
            title = SPApp.e.getNom() +" "+ SPApp.e.getPrenom() + " : Inscriptions aux UE";
        }
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("EtudiantGUI.fxml")), v, v1);
        stage.setScene(scene);
        stage.setTitle(title);
        Image img = new Image("UPS.jpg");
        stage.getIcons().add(img);
        stage.show();
    }


    public void handleBtDelete(MouseEvent e) throws IOException {
        MainClass.liEtu.remove(SPApp.e);
        SPApp.e = null;
        MainClass.writeOnCSV();
        sta.close();
    }


    public void handleBtSave(ActionEvent e) {
        try {
            save();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sauvegarde Réussie");
            alert.setContentText("Sauvergarde effectuée avec succès");
            alert.show();
            hasBeenSaved = true;
        } catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Sauvegarde echouée");
            alert.setContentText("La sauvegarde a échoué");
        }

    }


    public void handlebtAdd(MouseEvent e) throws IOException {
        if (btAdd.getText().equals("Ajouter")) {
            if (tfINE.getText().length() > 0 && tfNom.getText().length() > 0 && tfPrenom.getText().length() > 0) {
                Etudiant et = new Etudiant(tfNom.getText(), tfPrenom.getText(), tfINE.getText(), cbParcours.getValue() == null ? "" : cbParcours.getValue().getId());
                MainClass.liEtu.add(et);
                MainClass.writeOnCSV();
                sta.close();
            }
        } else {
            SPApp.e.setINE(tfINE.getText());
            SPApp.e.setNom(tfNom.getText());
            SPApp.e.setPrenom(tfPrenom.getText());
            SPApp.e.setIdParcours(cbParcours.getValue() == null ? "" : cbParcours.getValue().getId());
            MainClass.writeOnCSV();
            sta.close();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        cbParcours.getItems().addAll(FXCollections.observableList(MainClass.liParcours));
        if (SPApp.e != null) {
            tfINE.setText(SPApp.e.getINE());
            tfPrenom.setText(SPApp.e.getPrenom());
            tfNom.setText(SPApp.e.getNom());
            btAdd.setText("Modifier");
            btDelete.setVisible(true);
            loaddgv();
            for (Parcours p : MainClass.liParcours) {
                if (SPApp.e.getIdParcours().equals(p.getId())) {
                    cbParcours.getSelectionModel().select(p);
                    break;
                }
            }
        } else {
            tv.setVisible(false);
            btSave.setVisible(false);
        }
        sta.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                if (!hasBeenSaved && SPApp.e != null) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Fermeture de la fenêtre");
                    alert.setContentText("Etes-vous sûr de vouloir fermer sans sauvegarder ?");
                    Optional<ButtonType> result = alert.showAndWait();
                    if (!((result.isPresent()) && (result.get() == ButtonType.OK))) {
                        we.consume();
                    }
                }
            }
        });
    }

    /**
     * Charge la tableView
     */
    private void loaddgv() {

        tv.getItems().removeAll();
        tv.setEditable(true);
        ColUE.setCellValueFactory(new PropertyValueFactory<UEInscription, String>("nom"));
        ColUE.setEditable(true);
        ColECT.setCellValueFactory(new PropertyValueFactory<UEInscription, String>("creditECT"));
        ColParcours.setCellValueFactory(cellData -> {
            String parcoursName = UE.getUEParcoursName(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });

        ColPre.setCellValueFactory(cellData -> {
            String parcoursName = UE.getPrerequisName(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });
        ColMention.setCellValueFactory(cellData -> {
            String parcoursName = UE.getUEMentionName(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });

        ColInscrire.setCellValueFactory(new PropertyValueFactory<UEInscription, Boolean>("isInscrit"));
        ColInscrire.setCellFactory(tc -> new CheckBoxTableCell<>());
        ColInscrire.setEditable(true);
        tv.setItems(FXCollections.observableList(UEInscription.getUEInscriptions(SPApp.e.getUEEnvisageable())));
    }

    /**
     * Enregistre les modifications sur la grille
     *
     * @throws IOException
     */
    public void save() throws IOException {
        List<UEInscription> liuei = tv.getItems();
        for (UEInscription uei : liuei) {
            if (uei.getIsInscrit()) {
                SPApp.e.addUEEnCours(uei.getCodeUE());
            }
        }
        MainClass.writeOnCSV();
    }


}
