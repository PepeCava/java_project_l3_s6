package GUI;

import CSVReader.*;
import CSVWriter.*;
import Etudiant.Etudiant;
import Formation.Formation;
import Formation.Mention;
import Formation.Parcours;
import Formation.UE;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * JavaFX App
 */
public class MainClass extends Application {
    @FXML
    public Button btDE;
    @FXML
    public Button btSP;
    @FXML
    public Button btJ;

    private Parent root;
    public static String pathCsv;
    public static List<Etudiant> liEtu;
    public static List<Formation> liFormation;
    public static List<Mention> liMention;
    public static List<Parcours> liParcours;
    public static List<UE> liUE;
    private static Stage stage;
    public static String currentSemester;
    public static String currentYear;


    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        root = FXMLLoader.load(getClass().getResource("MainClass.fxml"));
        Scene scene = new Scene(root, 601, 377);
        primaryStage.setTitle("Gestionnaire de formation à la carte");
        primaryStage.setScene(scene);
        Image img = new Image("UPS.jpg");
        primaryStage.getIcons().add(img);
        primaryStage.show();
        // DirectoryChooser to select csv folder
        if (Files.exists(Paths.get(System.getProperty("user.dir") + "\\.path.txt"))) {
            File myObj = new File(System.getProperty("user.dir") + "\\.path.txt");
            Scanner myReader = new Scanner(myObj);
            pathCsv = myReader.nextLine();
            myReader.close();
        } else {
            pathChooser();
        }
        loadLists();
        defineDate();

    }


    public static void main(String[] args) throws Exception {
        launch();

    }

    /**
     * Charge la scène de la fenêtre principale
     * @param fxml Path du fichier fxml
     * @throws IOException
     */
    public static void setRoot(String fxml) throws IOException {
        stage.setScene(new Scene(FXMLLoader.load(MainClass.class.getResource(fxml)), 601, 377));
    }

    @FXML
    private void handleButtonSPAction(ActionEvent event) throws IOException {
        setRoot("SPApp.fxml");
    }

    @FXML
    private void handleButtonDEAction(ActionEvent event) throws IOException {
        setRoot("DEApp.fxml");
    }

    @FXML
    private void handleButtonBEAction(ActionEvent event) throws IOException {
        setRoot("BEApp.fxml");
    }

    @FXML
    private void handleSaveMenuItem(ActionEvent event) throws IOException {

    }

    @FXML
    private void handleRefreshMenuItem(ActionEvent event) throws IOException {

    }

    @FXML
    private void handleCloseMenuItem(ActionEvent event) throws IOException {
        Platform.exit();
    }

    @FXML
    private void handleChangePathMenuItem(ActionEvent event) throws IOException {
        pathChooser();
    }

    /**
     * Crée les listes en fonctions des CSV
     */
    public static void loadLists() {
        if (pathCsv.length() > 0) {
            liEtu = new CSVReaderEtudiant(pathCsv + "\\etudiant.csv").loadEtudiants();
            liFormation = new CSVReaderFormation(pathCsv + "\\formation.csv").loadEtudiants();
            liMention = new CSVReaderMention(pathCsv + "\\mention.csv").loadEtudiants();
            liParcours = new CSVReaderParcours(pathCsv + "\\parcours.csv").loadEtudiants();
            liUE = new CSVReaderUE(pathCsv + "\\UE.csv").loadEtudiants();
            new CSVReaderUESuivies(pathCsv + "\\valider.csv").loadUESuivies(liEtu, liUE);
            new CSVReaderUEEnCours(pathCsv + "\\suivre.csv").loadUEEnCours(liEtu);
        } else {
            liEtu = new ArrayList<Etudiant>();
            liFormation = new ArrayList<Formation>();
            liMention = new ArrayList<Mention>();
            liParcours = new ArrayList<Parcours>();
            liUE = new ArrayList<UE>();
        }
    }

    /**
     * Permet de définir le path du dossier contenant les CSV
     */
    public static void pathChooser() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Choisir le dossier contenant les CSV");
        try {
            pathCsv = directoryChooser.showDialog(new Stage()).getPath();
            String dir = System.getProperty("user.dir");
            File fnew = new File(dir + "\\.path.txt");
            try {
                FileWriter f2 = new FileWriter(fnew, false);
                f2.write(pathCsv);
                f2.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (NullPointerException e) {
            if (pathCsv.length() == 0)
                pathCsv = "";
        }
    }

    /**
     * Ecrit les différentes listes dans leurs CSV respectifs
     * @throws IOException
     */
    public static void writeOnCSV() throws IOException {
        new CSVWriterEtudiant(MainClass.pathCsv + "\\etudiant.csv").writeEtudiant(liEtu);
        new CSVWriterUE(MainClass.pathCsv + "\\UE.csv").writeUE(liUE);
        new CSVWriterUEEnCours(MainClass.pathCsv + "\\suivre.csv").writeUEEnCours(liEtu);
        new CSVWriterUESuivies(MainClass.pathCsv + "\\valider.csv").writeUESuivies(liEtu);

    }

    /**
     * Définit la date et le semestre par rapport à la date actuelle
     */
    public static void defineDate() {
        LocalDateTime now = LocalDateTime.now();
        if (now.getDayOfMonth() <= 6) {
            currentSemester = "2";
            currentYear = (now.getYear() - 1) + "-" + now.getYear();
        } else if (now.getDayOfMonth() > 6) {
            currentSemester = "1";
            currentYear = now.getYear() + "-" + (now.getYear() + 1);
        }
    }


}
