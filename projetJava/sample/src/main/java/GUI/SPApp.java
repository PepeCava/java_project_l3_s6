package GUI;

import CSVWriter.CSVWriterEtudiant;
import Etudiant.Etudiant;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SPApp extends Application implements Initializable {

    @FXML
    public TableView tbET;
    @FXML
    public AnchorPane aP;
    @FXML
    public TableColumn<Etudiant, String> colINE;
    @FXML
    public TableColumn<Etudiant, String> colNom;
    @FXML
    public TableColumn<Etudiant, String> colPrenom;
    @FXML
    public TableColumn<Etudiant, String> colParcours;
    public static Etudiant e;


    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = (Parent) FXMLLoader.load(this.getClass().getResource("SPApp.fxml"));
        Scene scene = new Scene(root, 601.0D, 377.0D);
        primaryStage.setTitle("Secrétariat pédagogique");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    @FXML
    private void handleButtonReturnAction(ActionEvent event) throws IOException {
        e = null;
        MainClass.setRoot("MainClass.fxml");
    }

    @FXML
    private void handleChangePathMenuItem(ActionEvent event) throws IOException {
        MainClass.pathChooser();
    }
    @FXML
    private void handleCloseMenuItem(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    private void handleAddEtuButton(MouseEvent event) throws Exception {
        e = null;
        new EtudiantGUI().start(new Stage());
    }

    @FXML
    public void clickItem(MouseEvent event) throws Exception {
        if (event.getClickCount() == 2) {
            e = ((Etudiant) tbET.getSelectionModel().getSelectedItem());
            new EtudiantGUI().start(new Stage());
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dgvLoadEtu();
        tbET.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
                if(t1)
                    dgvLoadEtu();
            }
        });;
    }

    /**
     * Charge la tableView
     */
    public void dgvLoadEtu() {
        tbET.getItems().removeAll();
        colINE.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("INE"));
        colNom.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("nom"));
        colPrenom.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("prenom"));
        colParcours.setCellValueFactory(cellData -> {
            String parcoursName = Etudiant.getETParcoursName(cellData.getValue());
            return new SimpleStringProperty(parcoursName);
        });
        tbET.setItems(FXCollections.observableList(MainClass.liEtu));
    }
}
