module GUI {
    requires javafx.controls;
    requires javafx.fxml;

    opens Etudiant to javafx.fxml;
    opens Formation to javafx.xml;
    opens CSVReader to javafx.xml;
    opens CSVWriter to javafx.xml;
    exports GUI;
    exports Etudiant;
    exports Formation;
    exports CSVReader;
    exports CSVWriter;
    opens GUI to javafx.fxml, javafx.xml;
}